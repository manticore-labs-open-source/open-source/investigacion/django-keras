# Django y Keras

En este tutorial vamos a implementar un servidor web que permita integrar `Deep Learning` a través de las librerías de 
Keras.

## Repositorio
Si quieres tener accesso al proyecto completo visita el siguiente [enlace](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/django-keras)

## Prerrequisitos
* Tener instalado en tu ordenador Python 3.x. [Ve al enlace para descargarlo ](https://www.python.org/downloads/).
* Un editor de código, de preferencia un IDE como [`PyCharm`](https://www.jetbrains.com/es-es/pycharm/download/#section=linux).
* Sistema operativo basado en Ubuntu (de preferencia por la versatilidad de la linea de comandos). 
* Tener instalado [POSTMAN](https://www.postman.com/).
* Tener instalado el gestor de paquetes de python: `pip`.
    * Para GNU/Linux desde la terminal:
    
        ```shell script
          sudo apt install python3-pip
        ```
     * Para windows desde el powershell o el cmd (es probable que los ejecutes como administrador si ta problemas):
     
      
        ```shell script
            python -m pip install -U pip3
        ```
      
     * Para macOS desde la terminal:
      
        ```shell script
          brew install python3 
        ```
      

## Desarrollo

## Levantar el ambiente de desarrollo.

### Crear el entorno virutal.

Antes que nada vamos a crear un entorno virtual para `python`, en este entorno vamos a instalar todas las dependencias o 
librerías de manera local en nuestro proyecto. Para ello abriremos una terminal y haremos uso del comando `virtualenv`.
Si no tienes instalado `virtualenv` lo instalaremos con `pip3`:

```shell script
  sudo pip3 install virtualenv
```
Ejecutamos el comando dentro del directorio donde vamos a tener el proyecto. 

```shell script
    virtualenv entorno
```

Una vez creado nuestro entorno virtual tendremos nuestro directorio de la siguiente forma:
```text
.
├── entorno
│   ├── bin
│   ├── lib
│   └── pyvenv.cfg
└── README.md

```
Luego desde la terminal ejecutaremos lo siguiente para activar nuestro ambiente.
```shell script
     source entorno/bin/activate
```
Si se activo correctamente vamos a poder ver el nombre de nuestro entorno entre paréntesis por delante de la línea de comandos de la terminal.

```shell script
(entorno)  
```
Para desactivarlo:
```shell script
deactivate
```

#### NOTA:
Te puedes saltar todo este asunto de crear el entorno virtual e instalar las dependencias de manera global.
Pero pueden haber conflictos al instalarlas.

#### NOTA 2:
Puedes usar `PyCharm` para realizar todo este proceso de manera más simple, 
revisa el siguiente [tutorial](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html).


### Instalar dependecias
Vamos a instalar las siguientes dependencias:
```text
absl-py==0.7.1
astor==0.7.1
gast==0.2.2
grpcio==1.19.0
h5py==2.9.0
Keras==2.2.4
Keras-Applications==1.0.7
Keras-Preprocessing==1.0.9
Markdown==3.0.1
mock==2.0.0
numpy==1.16.0
pbr==5.1.3
Pillow==5.4.1
protobuf==3.7.0
PyYAML==5.1
scipy==1.2.1
six==1.12.0
tensorboard==1.13.1
tensorflow==1.13.1
tensorflow-estimator==1.13.0
termcolor==1.1.0
Werkzeug==0.14.1
Django~=3.0.6
``` 
Son bastantes ¿No crees?, entonces para que no sea tan agobiante instalarlas de una en una, vamos a copiar todas
esas dependencias y pegarlas en un nuevo archivo dentro de nuestro directorio, por defecto lo llamaremos `requirements.txt`.

Ahora solo resta instalarlas a través de pip:

```shell script
pip3 install -r requirements.txt
```

### Crear el servidor web.
Vamos a crear nuestro servidor web a través del framework de `Django`.

```shell script
django-admin.py startproject mlabApp
```

Entonces una vez creado el proyecto de `Django` dentro de nuestro directorio tendremos algo como esto:
```text
.
├── entorno
│   ├── bin
│   ├── lib
│   └── pyvenv.cfg
├── mlabApp
│   ├── manage.py
│   └── mlabApp
├── README.md
└── requirements.txt

```
Ahora dentro de nuestro de nuestro proyecto `Django`, vamos a crear un nueva `app` (un módulo):
Creamos un directorio para los módulos:

```shell script
  mkdir modulos
```

Dentro del directorio `modulos`:

```shell script
 django-admin startapp deepLearning
```

Tendremos la siguiente estructura de directorios:

```text
└── modulos
    └── deepLearning
        ├── admin.py
        ├── apps.py
        ├── __init__.py
        ├── migrations
        ├── models.py
        ├── tests.py
        └── views.py

```


Agregamos nuestro modulo creado al proyecto, para ello vamos al archivo `settings.py`.

```python

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'modulos.deepLearning' ## Nuestro mádulo
]
```
También al final de este archivo se va a definir en que directorio se van a guardar los archivos que se van 
a subir.

```python
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
```

Es necesario tener el directorio, entonces dentro del directorio raíz de proyecto creamos el directorio `media` y dentro de el se crea la carpeta `imagenes`:

```text
├── mlabApp
│   ├── db.sqlite3
│   ├── manage.py
│   ├── media
│   │   └── imagenes
│   ├── mlabApp
│   │   ├── asgi.py
│   │   ├── __init__.py
│   │   ├── __pycache__
│   │   ├── settings.py
│   │   ├── urls.py
│   │   └── wsgi.py
│   └── modulos
│       └── deepLearning

```



## Implementar la red neural
Implementar una red neural desde cero aun con librerías es un proceso complejo. Sin embargo, 
para hacer este tutorial lo mas simple posible vamos a utilizar un modelo pre-entrenado el `VGG16`, entonces dentro de nuestro
módulo `deepLearning` crearemos un archivo en donde vamos a importar este modelo pre-entrenado e implementar la lógica de 
predicción con este modelo.

Importamos las dependencias necesarias con el modelo pre-entrenado incluido:
```python
from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input, decode_predictions
from keras import backend as K
import numpy as np
```

Luego implementamos nuestra lógica de predicción a través de la siguiente función:

```python
# Predecir a traves del modelo pre-entrenado: VGG16
def predecir_vgg16(ruta_imagen='glue_sticks.jpg'):
    K.clear_session()
    modelo_red_neural = VGG16(weights='imagenet')
    imagen_cargada = image.load_img(ruta_imagen, target_size=(224, 224))
    x = image.img_to_array(imagen_cargada)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    predicciones = modelo_red_neural.predict(x)
    resultados = decode_predictions(predicciones, top=3)[0][0]
    prediccion = resultados[1]
    presicion = resultados[2]
    return {
        'prediccion': prediccion,
        'precision': presicion * 100,
        'url_imagen': str(ruta_imagen)
    }
```

## Definir vistas (controladores o `views`):



Dentro del archivo `views.py` vamos a crear la siguiente función la cual va a manejar la petición `HTTP` y orquestará la predicción.

Antes que nada vamos a crear otro archivo llamado `manejadores.py` en el vamos a crear una función para
manejar la subida del archivo al servidor y retornaremos la `url` o ruta de la imagen que se ha guardado

Archivo `manejadores.py`:
En esta función definimos en que ruta se va a guardar la imagen y con que nombre se a subir.
De igual forma podemos crear otra función para subir las imágenes a un servidor de archivos o en una nube
como lo puede ser `google-cloud`.

```python
import uuid

def manejar_subida_archivo(f):
   # Definimos la ruta donde se va guardar el archivo y que nombre va a tener.
    ruta_imagen = 'media/imagenes/{}{}'.format(f.name, uuid.uuid4())
   # A partir de la ruta definida se va guardar el archivo parte por parte.
    with open(ruta_imagen, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
        return ruta_imagen

```


Archivo: `views.py`:

```python
from django.views.decorators.csrf import csrf_exempt
from .predecir import predecir_vgg16
from .manejadores import manejar_subida_archivo
from django.http import JsonResponse


# Vista para predicir una imagen
@csrf_exempt
def predecir_imagen(request):
    if request.method == 'POST':
        imagen = request.FILES['imagen']
        ruta_imagen = manejar_subida_archivo(imagen)
        resultados = predecir_vgg16(ruta_imagen)
        return JsonResponse(resultados)
    return JsonResponse({'mensaje': 'error'})

```

Ahora que tenemos definido nuestra vista vamos a asignarle una ruta o `endpoint`, 
dentro del archivo `urls.py` (Si no existe lo creamos) se hará lo siguiente:

```python
from django.urls import path
from .views import *

urlpatterns = [
    path('v1/predecir-imagen', predecir_imagen, name='predecir-imagen'),
]
```

> Toda esta codificación se la esta haciendo dentro del módulo: `deepLearning`

Dentro del archivo `urls.py` del directorio raiz del proyecto, se va a referenciar el `endoint` que definimos anteriormente: 

```python
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    # Estamos importando el enpoint y le estamos asignado al segmento `deeplearning`
    # /deeplearning/v1/predecir-imagen
    path('deeplearning/', include(('modulos.deepLearning.urls', 'deeplearning'), namespace="deeplearning")),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
``` 

### Levantar el servidor web.

Para poder consumir la `API` que hemos desarrollado vamos a modificar nuestro archivo `settings.py` para poder definir desde
que sitios vamos a llamar a la `API`:

```python
# Por defecto vamos a poner un asterisco para poder consumir la API desde cualquier lugar.
ALLOWED_HOSTS = ['*'] # Se suele poner una lista de direcciones IP.
```

Para levantar el servidor web dentro del directorio raíz del proyecto de `Django` ejecutamos el siguiente comando:
```shell script
python manage.py runserver
```

### Probar nuestra API de predicción de imágenes
Por defecto nuestro servidor va a correr en el siguiente host:
```text
http://127.0.0.1:8000/
```
Entonces nuestra  `uri` para consumir la API será el siguiente:
```text
POST http://127.0.0.1:8000/deeplearning/v1/predecir-imagen 
``` 



Vamos predecir la siguiente imagen:

![gatito](https://img2.freepng.es/20180205/sle/kisspng-cat-food-kitten-dog-adorable-cat-png-5a78cf7f8e6145.1072457615178668795832.jpg)



Desde POSTMAN vamos a probar la API con la imagen:
![image](https://gitlab.com/manticore-labs-open-source/open-source/investigacion/django-keras/uploads/33eff2f4dc43d2613c3657e979d3b09b/image.png)


#### Resultados
```json
{
    "prediccion": "Egyptian_cat",
    "precision": 55.444759130477905,
    "url_imagen": "media/imagenes/gatito.jpg834a4008-3f29-4c20-b52f-9e53b7cca384"
}
```

## Conclusiones
* Como se pudo ver en el tutorial, se implemento una `API` para predecir imagenes de manera sencilla
dado que estamos usando un modelo `preentrenado` pero para cosas de la `vida real` ya es necesario usar una red neural
entrenada.
* Podemos escalar más la aplicación usando el ORM de `Django` para poder guardar las predicciones.


### Consideraciones 
Puede ser probable que cuando estemos consumiendo la `API` por primera vez, el servidor web se
ponga a descargar el modelo pre-entrenado así que tener un poco de paciencia.


## Autor
Andrés Velasco.

* [Github](https://github.com/velascoandrs)
* [GitLab](https://gitlab.com/velascoandrs)
* [Linkedin](https://www.linkedin.com/in/andr%C3%A9s-velasco-210303135/)