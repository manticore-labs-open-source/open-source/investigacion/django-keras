import uuid


def manejar_subida_archivo(f):
    # Definimos la ruta donde se va guardar el archivo y que nombre va a tener.
    ruta_imagen = 'media/imagenes/{}{}'.format(f.name, uuid.uuid4())
    # A partir de la ruta definida se va guardar el archivo parte por parte.
    with open(ruta_imagen, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
        return ruta_imagen
