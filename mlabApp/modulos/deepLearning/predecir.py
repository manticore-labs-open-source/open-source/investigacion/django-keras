from keras.applications.vgg16 import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input, decode_predictions
from keras import backend as K
import numpy as np


# Predecir a traves del modelo pre-entrenado: VGG16
def predecir_vgg16(ruta_imagen='glue_sticks.jpg'):
    K.clear_session()
    modelo_red_neural = VGG16(weights='imagenet')
    imagen_cargada = image.load_img(ruta_imagen, target_size=(224, 224))
    x = image.img_to_array(imagen_cargada)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    predicciones = modelo_red_neural.predict(x)
    resultados = decode_predictions(predicciones, top=3)[0][0]
    prediccion = resultados[1]
    presicion = resultados[2]
    return {
        'prediccion': prediccion,
        'precision': presicion * 100,
        'url_imagen': str(ruta_imagen)
    }

# print(predecir_vgg16())
