from django.urls import path
from .views import *

urlpatterns = [
    path('v1/predecir-imagen', predecir_imagen, name='predecir-imagen'),
]