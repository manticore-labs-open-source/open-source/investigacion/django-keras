from django.views.decorators.csrf import csrf_exempt
from .predecir import predecir_vgg16
from .manejadores import manejar_subida_archivo
from django.http import JsonResponse


# Vista para predicir una imagen
@csrf_exempt
def predecir_imagen(request):
    if request.method == 'POST':
        imagen = request.FILES['imagen']
        ruta_imagen = manejar_subida_archivo(imagen)
        resultados = predecir_vgg16(ruta_imagen)
        return JsonResponse(resultados)
    return JsonResponse({'mensaje': 'error'})
